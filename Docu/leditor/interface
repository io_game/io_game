
* LEVEL EDITOR INTERFACE

  It consistis of two panels and the rest of the screen is destinated to
  the preview of the level being created (edtition area).
  
  Note.- The coordinate system used in this documentation file is with the 
  origin (0,0) at the top left corner, it may differ with the one used in the
  code.
** INTERFACE CURRENT STATE
   
   In order for the editor to register which state the interface is, there
   it is being implemented a defined structure.

   See [[file:interface_current_state][Docu/leditor/interface_current_state]] to see the states and what
   actions will be taken.

** LEFT PANEL
   
   It consists of eigth buttons, each button has an icon that simbolizes 
   the button's function, this is a tool button panel.
   By clicking or touching them the function is activated. The icons 
   images are defined in:
   
   "leditor/images/icons"
   
   From (0, 50) to (50,464) it contains a total of 8 buttons represented
   by icons.
   
*** Selection button
    
    
    Has a reserved rectangle area from (1,51) to (49,99) to render the icon.
    By selecting, the selection mode is activated, when touching an object
    in the edition area it is selected and can perform actions over the
    selected object.
    To deselect an object it is ok to select onother one or select other tool
    button non realted to object selection.
    
*** Delete button
    
    Has a reserved rectangle area from (1,103) to (49,51) to render the icon.
    By selecting it and then selecting an object of the edition area the
    object touched is removed from the level.
    
*** Duplicate button
    
    Has a reserved rectangle area from (1,155) to (49,203) to render the icon
    By selecting it, lets duplicate a selected object and put it in a selected
    point.
    
***  Rotate button
     
     Has a reserved rectangle area from (1,207) to (49,255) to render the icon
     By selecting it, it helps to rotate a selected object.
     
***  Move button
     
     Has a reserved rectangle area from (1,259) to (49,307) to render the icon
     By selecting it, a selected object can be moved to a valid point of the
     edition area. A valid point is where no other object collides to the 
     moved object.
     
*** Polygon button
    
    Has a reserved rectangle area from (1,311) to (49,359) to render the icon.
    By selecting it, the user can draw a polygon by placing vertex points. 
    The polygon must have at least 3 vertexes in a valid position, after the
    3rd vertex point placed the lines start to draw automatically to fill a
    polygon and the user can finish to draw the polygon by selecting the
    auxiliar button that has changed the icon to do so.
    
*** Complements button
    
    Has a reserved rectangle area from (1,363) to (49,411) to render the icon
    By selecting it, will show a panel showing available complements. A 
    complement is an object non collidable that goes in the background such
    as stars, clouds, mountanins, etc. Once the user has choosen the desired 
    complement it can now be put at any point of the editiion area to make the
    level look better.
    
*** Powerups button
    
    Has a reserved rectangle area from (1,415) to (49,463) to render the icon
    By selecting it, will show a panel displaying the available powerups.
    A powerup is a special game object that can be colected when Io makes
    contact with them.
    The powerups are special powers or bonuses that can be used later,
    the user can put a powerup in given point of the level.
    A powerup must not collide with another collidable object.
    There must be at least two powerups, the start portal and the finish
    portal in a level screen.
    
** SUPERIOR PANEL    
   
   A panel that has a reserved area at the top of the screen, the superior 
   panel has two buttons and a message area.
   
*** File Button
    
    This has a size of 48x48 pixels to contain an icon.
    It takes a rectangle that starts at (1,1) and finishes at (49,49).
    When it is selected, opens a file menu that contains some other options
    to work on the level file:
    
**** New
     
     Creates a blank new level and loads it, if there is already opened another
     unsaved level the editor asks before closing the current level.
     
**** Open
     
     Loads an existing level file, if there is already opened another unsaved 
     level the editor asks before closing the current level.
     To select an existing level it is displayed a submenu whith the name of
     the levels. The default folder being shown is:
     "leditor/levels/custom" where by default should be the player's customized 
     level and some default examples. The editor should be able to read into
     directories and look for files that have the extension ".iol". 
     In order to do so it is also necessary to implement a tiny file
     navigator with the option to create folders and move through them without 
     going outside the game's folder scope.
     
**** Save as
     
     To save the level by giving a name to it. The saved level is stored 
     in a file which name must be specified. If there exists a file with the 
     name already, the editor asks before overwriting it. 
     To put a name to the level it is mandatory to show an interactive keyboard
     or open the plataform's default. No matter the name the editor will always 
     put a ".iol" string to specify the extension.
     
**** line separator
     
**** Level Info
     
     To specify the level metadata including:
     
	* Autor's name and contact
	* Level name
	* A short descriptions
	* The available languages in the level
	  
     Each level can have multiple languages to show its text. The player can
     write the text of the lesson or the info using the language he/she desires,
     the only condition is to specify the language's name for instance "Español"
     or "English" In the level info so whoever plays it will know what language
     is used. 
     There is also the possibility to write each level text using multiple
     languages.
     
     There is also the possibility to specify the "next level" that will 
     be loaded after finishing this one. This option is not a must.
     To specify the "next level", the user has to select an existing level
     file. Once done the main game will try to load the specified level by
     searching that filename and load into a game level, if goes well the game
     will continue in the level specified. If it fails the game will show an
     error message. 
     
     This last functionality will permit someone to create a subworld that Io
     can discover and can share with bother people :)
     
     
**** Level lessons
     
     Here the player can edit the level's lesson by writing a question and
     three options with one of them being the correct option.
     There should be an explanation about why that one is the correct option 
     to choose. The question and explanation's lenght can be arbitrary
     (because in the game there must be implemented a tiny text viewer that 
     lets the player read large text content by changing pages). 
     Once the user has finished to wirte the text above a "finish button" is 
     to be pressed. After pressed that button the editor will show a text box
     to write the language's name used for the level. Once the language is 
     specified, the editor will ask if the player wants to add a translation 
     in another language fot the level.
     
     If the user desires, a prize can be given after finishing the level.
     
**** separator
     
**** TEST
     
     This option can be selected to test the level (load the current level on
     the main game in order to test it), it is mandatory to save the level
     before doing so.
     Before loading the level, the editor checks if there are errors such 
     as missing "level's info" or "level's lesson", it is also performed an
     algorithm that will check if all objects are correctly placed and it will
     also check wether it is possible to finish the level. If there is no way
     to finish the level using 15 "bounces" the editor will complain asking to 
     add powerups or to modify the level to make it possible for a player
     to win (aka reach the exit portal)
     
**** QUIT
     
     To exit the editor and return to the Main Menu Screen, it will ask
     to save the current level being edited if there have been changes after
     saved. Then it will release all resources used by the level editor.
     
*** Messages Area
    
    A reserved area destinated to show helpful texto to the user, 
    such as "draw points" or "invalid poligon: 
    (must have at least three points)".
    The area has a reserved rectangle space from (53,2) to (747,47).
    In this area the editor will show information about what is being done, for
    instance when the selection button is selected, in this area there will be
    shown the text "select the object to edit".
    These messages are to be translated from english to other languages so 
    the editor will be multilanguage.
    If there is the case that the text does not fit in this area of the screen
    a *--small arrow will appear to show the next section of the text 
    (For this to be done it is necessary a way to detect the text lenght and
    see if it fits on that space). There will also appear an small arrow at the
    left of this box to move to the previous section(s)--*.
    
*** Auxiliar button
    
    This button has a reserved rectangle in the screen from (750,1) to (799,49).
    The function of this button is to change according to the context.
    An example of this is when an object of the level is selected the icon in
    this area will change to the "properties" icon. So if the user clicks on it
    a menu of the object's propierties will be displayed. This will let the 
    user to change the object's properties, in the case of a polygon a property
    that can be changed is the body color or the line color.
    
    When nothing is selected the buttons will show the "change_screen" button
    to other screen of the level (A level can contain multiple screens too, all
    of them are connected through a special powerup called local portal, this 
    powerup transports Io to the next screen of the same level).
    
    If there is just one screen and this button is selected the editor will ask 
    if the user wants to add another screen. The number of screens for a level
    can be 1 to 12 (maybe more). {Do each screen should contain lesson too? }
    
** EDITION AREA
   
   In this is shown the level being edited, all objects are rendered here.
   The area takes a rectangle from (50,50) to (800,480)
   
   Because the superior and left panel take some area of the screen, 
   the level being edited has a total of 749x429.
   
   *) Each screen of a level is 800x430 in the game, to save a level
   it is necessary to transform the 750x430 size to the original size of a 
   level (800x430).
