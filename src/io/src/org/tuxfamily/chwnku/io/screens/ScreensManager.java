/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/** An object that manages and may contain all screens in the game, it has indexed other
 *  screens and can switch between them.*/
public class ScreensManager {
	public static final String TAG = ScreensManager.class.getName();
	public static final ScreensManager instance = new ScreensManager();
	
	public MainMenuScreen main_menu_screen;
	public HelpScreen help_screen;
	public LevelEditorScreen level_editor_screen;
	public PlayScreen play_screen;
	
	// singleton
	public ScreensManager() {	}
	
	public void init()
	{
		main_menu_screen = new MainMenuScreen();
		help_screen = new HelpScreen();
		level_editor_screen = new LevelEditorScreen();
		play_screen = new PlayScreen();
		ChangeToMainMenu();
	}
	
	/** Renders the current active screen */
	public void RenderCurrent (SpriteBatch batch)
	{
		if(this.main_menu_screen.visible())
			this.main_menu_screen.render(batch);
		else if(this.help_screen.visible())
			this.help_screen.render(batch);
		else if(this.level_editor_screen.visible())
			this.level_editor_screen.render(batch);
	}

	/** Updates the state of the current screen */
	public void UpdateCurrent (float delta) 
	{
		if(this.main_menu_screen.visible())
			this.main_menu_screen.update(delta);
		else if(this.help_screen.visible())
			this.help_screen.update(delta);
		else if(this.level_editor_screen.visible())
			this.level_editor_screen.update(delta);
	}

	/** Makes the MainMenuScreen visible and all others hidden */
	public void ChangeToMainMenu ()
	{
		Gdx.app.log(TAG, "MainMenuScreen activated");
		//this.main_menu_screen.LoadAssets(); //implement a way to load assets only once (!)
		this.DisposeLevelEditor();	// the LevelEditorScreen resources are not needed
		
		this.main_menu_screen.show();
		this.level_editor_screen.hide();
		this.help_screen.hide();
		this.play_screen.hide();
	}

	/** Makes the HelpScreen visible and all others hidden */
	public void ChangeToHelp () 
	{
		Gdx.app.log(TAG, "HelpScreen activated");
		this.main_menu_screen.hide();
		this.level_editor_screen.hide();
		this.help_screen.show();
		this.play_screen.hide();
	}
	
	/** Makes the LevelEditor visible and all others hidden */
	public void ChangeToLevelEditor () 
	{
		this.DisposeMainMenu();	// the MainMenuScreen resources are not needed
		this.level_editor_screen.LoadAssets();
		
		this.main_menu_screen.hide();
		this.level_editor_screen.show();
		this.help_screen.hide();
		this.play_screen.hide();
		Gdx.app.log(TAG, "LevelEditorScreen activated");
	}
	
	/** Makes the PlayScreen visible and all others hidden */
	public void ChangeToPlay()
	{
		this.DisposeMainMenu();	// the MainMenuScreen resources are not needed
		this.main_menu_screen.hide();
		this.level_editor_screen.hide();
		this.help_screen.hide();
		this.play_screen.show();
		Gdx.app.log(TAG, "PlayScreen activated");
	}
	
	/** Releases all resources being used by MainMenuScreen */
	public void DisposeMainMenu () 
	{
		this.main_menu_screen.dispose();
	}

	/** Releases all resources being used by HelpScreen */
	public void DisposeHelp () 
	{
		
	}

	/** Releases all resources being used by LevelEditorScreen */
	public void DisposeLevelEditor () 
	{
		
	}

}
