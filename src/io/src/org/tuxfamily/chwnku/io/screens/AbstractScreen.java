/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.tuxfamily.chwnku.io.screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class AbstractScreen {
	
public AbstractScreen() {
		
	}

	/** Called to make the screen render itself */
	public abstract void render (SpriteBatch batch);

	/** It is called to resize the screen */
	public abstract void resize (int width, int height);

	/** When called this screen becomes the one current screen of the game */
	public abstract void show ();

	/** Called to make this screen not to show and so it is no longer the current screen of the game */
	public abstract void hide ();

	/** Called when the pause event in android starts */
	public abstract void pause ();

	/** to resume the game after being paused */
	public abstract void resume ();

	/** To release all resources being currently used by this screen */
	public abstract void dispose ();
	
	/** Updates the screen state given a delta time in seconds*/
	public abstract void update (float delta);
}
