/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.screens;

import java.awt.Point;
import java.util.Vector;

import org.tuxfamily.chwnku.io.screens.AbstractScreen;
import org.tuxfamily.chwnku.io.utilities.Ut;
import org.tuxfamily.chwnku.io.Levels.LevelEditorInterface;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/** The Level editor screen, it is a manager class that has the function to catch 
 * <p> input events and call the level editor interface to manage them. */
class LevelEditorScreen extends AbstractScreen {
	public static final String TAG = LevelEditorScreen.class.getName();
	private boolean visible;
	private LevelEditorInterface Interface;

	private int sprsnumber;
	
	protected boolean pressed;
	protected boolean released;
	
	public LevelEditorScreen() 
	{
		visible = false;
		Interface = new LevelEditorInterface();
	}

	public void LoadAssets()
	{
		Interface.LoadAssets();
	}

	@Override
	public void render(SpriteBatch batch)
	{
		Interface.RenderWidgets(batch);
		Interface.RenderEffects(batch);
		Interface.RenderText(batch);
	}

	@Override
	public void resize(int width, int height)
	{
		// TODO Auto-generated method stub
		Point p = new Point();
	}

	@Override
	public void show() 
	{
		this.visible = true;
	}

	@Override
	public void hide()
	{
		this.visible = false;
	}

	@Override
	public void update(float delta) 
	{
		int X = Ut.TopLeftToMidCoordX((int)Ut.GetRelativeX());
		int Y = Ut.TopLeftToMidCoordY((int)Ut.GetRealitveY());
		// --  test for pressed button --
		if(Gdx.input.isTouched())
		{
			pressed = true;
			if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(50), Ut.TopLeftToMidCoordY(50), 750, 430))
			{	// Inside edition area
				Interface.EditionAreaPressedEffect(X, Y);
			}
			else
			{	// Inside panels
				Interface.ButtonPressedEffect(X, Y);
			}
		}
		else
		{
			Interface.NotTouched();
			if(pressed)
			{
				released= true;
				pressed = false;
			}
		}
		// ----
		// -- test for released button --
		if(released)
		{
			Interface.TestSelected(X,Y);
			released = false;
		}
		// ----
		
	}

	@Override
	public void pause() 
	{
		// TODO Auto-generated method stub	
	}

	@Override
	public void resume() 
	{
		// TODO Auto-generated method stub	 
	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
	}

	/** Loads a level by a given filename. If success returns true. */
	public static boolean LoadLevel (String filename) 
	{
		return true;
	}

	/** Checks if the level is valid and then saves it */
	public static boolean SaveLevel (String LevelName)
	{
		return false;
	}

	/** Displays basic help of the level editor */
	public static void DisplayHelp ()
	{

	}	

	public boolean visible() { return this.visible; }
	
}