/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.screens;

import java.util.Random;

import org.tuxfamily.chwnku.io.game.Assets;
import org.tuxfamily.chwnku.io.utilities.LanguageText;
import org.tuxfamily.chwnku.io.utilities.StandarValues;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class MainMenuScreen extends AbstractScreen{
	public static String TAG = MainMenuScreen.class.getName();
	private boolean visible = false;
	
	private int spritesnumber;
	private int SprEffect1;	// pointer to the flash button effect
	private int SprBird1_1;	// pointer to the sprite bird1-1
	private int SprBird1_2; // pointer to the sprite bird1-2
	private int SprBird1_3; // pointer to the sprite bird1-3
	private int SprBird2_1; // pointer to the sprite bird2-1
	private int SprBird2_2; // pointer to the sprite bird2-2
	private int SprBird2_3; // pointer to the sprite bird2-3
	private int SprBee1_1;	// pointer to the sprite bee1_1
	//private int SprEffect4; 

	private boolean bird1 = true; // if the bird1 effect is currently being drawn
	private byte bird1_current = 0;	// the pointer to the bird animation current sprite
	private boolean bird2 = true;
	private byte bird2_current = 0;
	private float animation1_change_time = 0.5f; // time required for bird1 animation to change to the next sprite.
	private float animation2_change_time = 0.37f;
	private float animation_bee_change_time = 16f;
	private boolean bee_to_leave_flower = false;	//reports if the bee has to leave the flower
	private boolean bee = true;	// if the bee animation effect is currently being drawn
	
	private boolean released = false;
	private boolean pres = false;

	private Array<Sprite> sprs_nm = new Array<Sprite>();
	private Array<Sprite> sprs = new Array<Sprite>();
	//private Vector<Sprite> sprs = new Vector<Sprite>();
	public MainMenuScreen()
	{
		//LanguageText.instance.LoadLanguage(LanguageText.instance.CodeName);
		LanguageText.instance.init();
		LoadAssets();
	}

	@Override
	public void render(SpriteBatch batch)
	{
		for(Sprite sprite: sprs_nm)
		{
			sprite.draw(batch);
		}
		for(Sprite sprite : sprs) 
		{ //draws all the loaded sprites in sprites[]
			sprite.draw(batch);
		}
		BitmapFont texto = Assets.instance.fonts.defaultNormal;
		// text for the menu screen
		texto.setColor(0.89f, 0.86f, 0.20f, 0.88f);
		texto.drawMultiLine(batch, LanguageText.instance.MainMenu_PlayButton, -235f, 134f); //play
		texto.drawMultiLine(batch, LanguageText.instance.MainMenu_LevelEditorButton, -235f, 74f); // level editor
		texto.drawMultiLine(batch, LanguageText.instance.MainMenu_OptionsButton, -235f, 14f); // options
		texto.drawMultiLine(batch, LanguageText.instance.MainMenu_QuitButton, -80f, -54f); // quit
		texto.drawMultiLine(batch, LanguageText.instance.MainMenu_HelpButton, 65f, 134f); // help
		texto.drawMultiLine(batch, LanguageText.instance.MainMenu_LanguageButton, 65f, 74f); // language
		texto.drawMultiLine(batch, LanguageText.instance.MainMenu_AboutButton, 65f, 14f); // about
	}

	@Override
	public void resize(int width, int height) 
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void show() 
	{
		this.visible = true;
	}

	@Override
	public void hide() 
	{
		this.visible = false;
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		sprs.clear();
		sprs_nm.clear();
	}

	@Override
	public void update(float delta) 
	{
		int Y = TopLeftToMidCoordY((int)GetRealitveY());
		int X = TopLeftToMidCoordX((int)GetRelativeX());
		Random rand = new Random();
		int ran = rand.nextInt();
		// updates the bird and bee effects
		EffectBird1(ran, delta);
		EffectBird2(ran, delta);
		EffectBee(ran, delta);
		
		if(Gdx.input.isTouched()) // to add effect
		{
			pres = true;
			//Gdx.app.log("Touched at", "X="+X + "Y="+Y);
			//Gdx.app.log("Touched ", "x="+Gdx.input.getX()+" y="+Gdx.input.getY()+" width="+Gdx.graphics.getWidth()+" height="+Gdx.graphics.getHeight()+" relx="+GetRelativeX()+" rely="+GetRealitveY());
			// tests if it is inside any button
			if(InsideRectangle(X, Y, -250, 100, 256, 48)) // play button being pressed
				RectangleTouchEffect(-250, 100, 256, 48);
			else if(InsideRectangle(X,Y,-250,40,256,48)) //level editor button being pressed
				RectangleTouchEffect(-250, 40, 256, 48);
			else if(InsideRectangle(X,Y,-250,-20,256,48)) //options button being pressed
				RectangleTouchEffect(-250, -20, 256, 48);
			else if(InsideRectangle(X, Y, -100, -80, 256, 48)) // quit button being pressed
				RectangleTouchEffect(-100, -80, 256, 48);
			else if(InsideRectangle(X, Y, 50, 100, 256, 48)) // help button being pressed
				RectangleTouchEffect(50, 100, 256, 48);
			else if(InsideRectangle(X, Y, 50, 40, 256, 48)) // language button being pressed
				RectangleTouchEffect(50, 40, 256, 48);
			else if(InsideRectangle(X, Y, 50, -20, 256, 48)) // about button being pressed
				RectangleTouchEffect(50, -20, 256, 48);			
		}
		else
		{	// dummy sprite
			Pixmap pixmap = new Pixmap(1, 1,Format.RGBA8888);
			pixmap.setColor(1,1,1,0);
			sprs.set(SprEffect1, new Sprite(new Texture(pixmap)));
			pixmap.dispose();
			if(pres)
			{
				released = true;
				pres = false;
			}

		}
		// -- here test for the released button --
		if(released) // was touched but is no longer being touched
		{
			if(InsideRectangle(X, Y, -250, 100, 256, 48)) // play button selected
			{	// switch to play screen
				ScreensManager.instance.ChangeToPlay();
				Gdx.app.log("Selected ", "play");
			}
			else if(InsideRectangle(X, Y, -250, 40, 256, 48)) // level editor button selected
			{	// switch to level editor screen
				ScreensManager.instance.ChangeToLevelEditor();
				Gdx.app.log("Selected ", "level editor");
			}
			else if(InsideRectangle(X,Y,-250,-20,256,48)) //options button selected
			{	// start the options screen
				Gdx.app.log("Selected ", "options");
			}
			else if(InsideRectangle(X, Y, -100, -80, 256, 48)) // quit button selected
			{	// finish the application and free all resources
				Gdx.app.log("Selected ", "quit");
				Gdx.app.exit(); // seems it does not finishes the app immediately
			}
			else if(InsideRectangle(X, Y, 50, 100, 256, 48)) // help button selected
			{	// switch to help menu screen
				ScreensManager.instance.ChangeToHelp();
				Gdx.app.log("Selected ", "help");
			}
			else if(InsideRectangle(X, Y, 50, 40, 256, 48)) // language button selected
			{	// loads the next available language
				Gdx.app.log("Selected ", "language");
				LanguageText.instance.ChangeToNextLanguage();
			}
			else if(InsideRectangle(X, Y, 50, -20, 256, 48)) // about button selected
			{	// switch to about screen
				Gdx.app.log("Selected ", "about");
			}
			released = false;
		}
		// ----

	}
	/** Loads the main menu screen assets to be rendered*/
	public void LoadAssets()
	{
		spritesnumber = -1;
		Array<TextureRegion> regions = new Array<TextureRegion>();
		regions.add(Assets.instance.menu_screen_bg.background);
		regions.add(Assets.instance.menu_screen_logo.logo);
		regions.add(Assets.instance.menu_screen_birds.bird1_1);
		regions.add(Assets.instance.menu_screen_birds.bird1_2);
		regions.add(Assets.instance.menu_screen_birds.bird1_3);
		regions.add(Assets.instance.menu_screen_birds.bird2_1);
		regions.add(Assets.instance.menu_screen_birds.bird2_2);
		regions.add(Assets.instance.menu_screen_birds.bird2_3);
		regions.add(Assets.instance.menu_screen_bg.cactus);
		regions.add(Assets.instance.menu_screen_bee.bee1_1);
		// background
		Sprite spr = new Sprite(regions.get(0));
		spr.setSize(800f, 480f);
		spr.setOrigin(spr.getWidth() / 2.0f, spr.getHeight() / 2.0f);
		spr.setPosition(TopLeftToMidCoordX(0f), TopLeftToMidCoordY(StandarValues.SCREEN_HEIGHT));
		// here code to test sprites functions
		sprs_nm.add(spr);
		// logo
		spr = new Sprite(regions.get(1));
		spr.setSize(88f, 48f);
		spr.setOrigin(spr.getWidth() / 2.0f, spr.getHeight() / 2.0f);
		spr.setPosition( -44f, 160f);
		sprs_nm.add(spr);
		// -- birds --
		// bird 1-1
		spr = new Sprite(regions.get(2));
		spr.setSize(14f, 14f);
		spr.setOrigin(spr.getWidth() / 2.0f, spr.getHeight() / 2.0f);
		spr.setPosition( TopLeftToMidCoordX(0), TopLeftToMidCoordY(28f));
		sprs.add(spr); SprBird1_1 = ++spritesnumber;
		// bird 1-2
		spr = new Sprite(regions.get(3));
		spr.setSize(14f, 14f);
		spr.setOrigin(spr.getWidth() / 2.0f, spr.getHeight() / 2.0f);
		spr.setPosition( TopLeftToMidCoordX(0), TopLeftToMidCoordY(28f));
		sprs.add(spr); SprBird1_2 = ++spritesnumber;
		// bird 1-3
		spr = new Sprite(regions.get(4));
		spr.setSize(14f, 14f);
		spr.setOrigin(spr.getWidth() / 2.0f, spr.getHeight() / 2.0f);
		spr.setPosition( TopLeftToMidCoordX(0), TopLeftToMidCoordY(28f));
		sprs.add(spr); SprBird1_3 = ++spritesnumber;
		// bird 2-1
		spr = new Sprite(regions.get(5));
		spr.setSize(22f, 22f);
		spr.setOrigin(spr.getWidth() / 2.0f, spr.getHeight() / 2.0f);
		spr.setPosition( TopLeftToMidCoordX(0), TopLeftToMidCoordY(334f));
		sprs.add(spr); SprBird2_1 = ++spritesnumber;
		// bird 2-2
		spr = new Sprite(regions.get(6));
		spr.setSize(22f, 22f);
		spr.setOrigin(spr.getWidth() / 2.0f, spr.getHeight() / 2.0f);
		spr.setPosition( TopLeftToMidCoordX(0), TopLeftToMidCoordY(334f));
		sprs.add(spr); SprBird2_2 = ++spritesnumber;
		spr = new Sprite(regions.get(7));
		spr.setSize(22f, 22f);
		spr.setOrigin(spr.getWidth() / 2.0f, spr.getHeight() / 2.0f);
		spr.setPosition( TopLeftToMidCoordX(0), TopLeftToMidCoordY(334f));
		sprs.add(spr); SprBird2_3 = ++spritesnumber;
		// ----
		// Io's cactus
		spr = new Sprite(regions.get(8));
		spr.setSize(242f, 421f);
		spr.setPosition(TopLeftToMidCoordX(0), TopLeftToMidCoordY(StandarValues.SCREEN_HEIGHT));
		sprs.add(spr); spritesnumber++;
		// bee
		spr = new Sprite(regions.get(9));
		spr.setSize(11f, 12f);
		spr.setPosition(TopLeftToMidCoordX(StandarValues.SCREEN_WIDTH+11), TopLeftToMidCoordY(286));
		sprs.add(spr); SprBee1_1 = ++spritesnumber;
		// ----
		// -- buttons --
		Pixmap pixmap = new Pixmap(1, 1,Format.RGBA8888);
		pixmap.setColor(0.15f, 0.47f, 0.05f, 0.61f);
		pixmap.fill();
		//pixmap.fillRectangle(21, 78, buttons_width, buttons_height);51f
		//pixmap.drawRectangle(21, 78, 14, 43);
		Texture texture = new Texture(pixmap);
		// play button
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(256, 48);
		sprs.peek().setPosition(-250f, 100f);
		sprs.add(new Sprite(texture)); ++spritesnumber; 
		sprs.peek().setSize(14, 48);
		sprs.peek().setPosition(-250f, 100f);
		// level editor button
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(256, 48);
		sprs.peek().setPosition(-250f, 40f);
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(14, 48);
		sprs.peek().setPosition(-250f, 40f);
		// options button
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(256, 48);
		sprs.peek().setPosition(-250f, -20f);
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(14, 48);
		sprs.peek().setPosition(-250f, -20f);
		// quit button
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(256, 48);
		sprs.peek().setPosition(-100f, -80f);
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(14, 48);
		sprs.peek().setPosition(-100f, -80f);
		// help button
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(256, 48);
		sprs.peek().setPosition(50, 100f);
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(14, 48);
		sprs.peek().setPosition(50, 100f);
		// language button
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(256, 48);
		sprs.peek().setPosition(50, 40f);
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(14, 48);
		sprs.peek().setPosition(50, 40f);
		// about button
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(256, 48);
		sprs.peek().setPosition(50, -20f);
		sprs.add(new Sprite(texture)); ++spritesnumber;
		sprs.peek().setSize(14, 48);
		sprs.peek().setPosition(50, -20f);
		// ----
		SprEffect1 = spritesnumber;
		pixmap.setColor(1,1,1,0f);
		texture = new Texture(pixmap);
		sprs.peek().setPosition(50, -20f);
		//texture.dispose();
		pixmap.dispose();
	}
	// -- special effects --
	/** Loads a clear rectangle to be rendered */
	private void RectangleTouchEffect(int x, int y, int width, int height )
	{
		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(0.22f, 0.30f, 0.44f, 0.85f);
		pixmap.fill();
		Texture texture = new Texture(pixmap);
		sprs.set(SprEffect1, new Sprite(texture));
		sprs.get(SprEffect1).setPosition(x, y);
		sprs.get(SprEffect1).setSize(width, height);
		pixmap.dispose();
		//texture.dispose();
	}

	/** Loads the animation of a bird flying very high in the background  */
	public void EffectBird1(int rand, float delta)
	{	// this bird has "reserved the first 45 pixels of the Y axis"
		if(bird1) // the effect has not have finished to be drawn
		{
			// SprBird1_1 is used as x, y reference position for the others
			int x=(int)sprs.get(SprBird1_1).getX();
			float y=sprs.get(SprBird1_1).getY();

			this.animation1_change_time -= delta;
			if(this.animation1_change_time <= 0) // changes the animation sprite
			{
				this.animation1_change_time = 0.5f;
				if(bird1_current==0)
				{
					sprs.get(SprBird1_2).setColor(1,1,1,1);
					bird1_current++;
					sprs.get(SprBird1_1).setColor(1,1,1,0);
					sprs.get(SprBird1_3).setColor(1,1,1,0);
				}
				else if(bird1_current == 1)
				{
					sprs.get(SprBird1_3).setColor(1,1,1,1);
					bird1_current++;
					sprs.get(SprBird1_1).setColor(1,1,1,0);
					sprs.get(SprBird1_2).setColor(1,1,1,0);
				}
				else if(bird1_current == 2)
				{
					sprs.get(SprBird1_1).setColor(1,1,1,1);
					bird1_current = 0;
					sprs.get(SprBird1_2).setColor(1,1,1,0);
					sprs.get(SprBird1_3).setColor(1,1,1,0);
				}
				float a=rand>1943? MathUtils.cos(x)*3: MathUtils.cos(x)*(-2);
				y += a;
				//Gdx.app.log(TAG, this.animation1_change_time+ "("+x+","+(int)y+"),a="+a);
			}
			x += 1;
			//y += MathUtils.cos(x/28); // costly operation
			sprs.get(SprBird1_1).setPosition(x, y);
			if(bird1_current == 1)
				sprs.get(SprBird1_2).setPosition(x, y);
			else if(bird1_current == 2)
				sprs.get(SprBird1_3).setPosition(x, y);
			// End of the animation
			if(x >= TopLeftToMidCoordX(StandarValues.SCREEN_WIDTH))
			{	// resets the animation's sprites
				bird1 = false;
				sprs.get(SprBird1_1).setPosition(TopLeftToMidCoordX(-26f), TopLeftToMidCoordY(rand%30+16));
				// makes them invisible
				sprs.get(SprBird1_1).setColor(1,1,1,0);
				sprs.get(SprBird1_2).setColor(1,1,1,0);
				sprs.get(SprBird1_3).setColor(1,1,1,0);
			}
		}
		else
		{
			if((rand > 1399333) && (rand < 10000000))
				bird1 = true;

		}
	}
	/** Loads the animation of some birds flying slowly near the middle of the screen*/
	public void EffectBird2(int rand, float delta)
	{
		if(bird2) // the animations has not finished yet
		{
			// Sprite Sprbird2_1 is used as reference for the others
			float x=sprs.get(SprBird2_1).getX();
			float y=sprs.get(SprBird2_1).getY();

			this.animation2_change_time -= delta;
			if(this.animation2_change_time <= 0) // changes the animation sprite
			{
				this.animation2_change_time = 0.37f;
				if(bird2_current==0)
				{
					sprs.get(SprBird2_2).setColor(1,1,1,1);
					bird2_current++;
					sprs.get(SprBird2_1).setColor(1,1,1,0);
					sprs.get(SprBird2_3).setColor(1,1,1,0);
				}
				else if(bird2_current == 1)
				{
					sprs.get(SprBird2_3).setColor(1,1,1,1);
					bird2_current++;
					sprs.get(SprBird2_1).setColor(1,1,1,0);
					sprs.get(SprBird2_2).setColor(1,1,1,0);
				}
				else if(bird2_current == 2)
				{
					sprs.get(SprBird2_1).setColor(1,1,1,1);
					bird2_current = 0;
					sprs.get(SprBird2_2).setColor(1,1,1,0);
					sprs.get(SprBird2_3).setColor(1,1,1,0);
				}
				//Gdx.app.log(TAG, this.animation1_change_time+ "("+x+","+(int)y+"),a="+a);
			}
			x += 0.34;
			sprs.get(SprBird2_1).setPosition(x, y);
			if(bird2_current == 1)
				sprs.get(SprBird2_2).setPosition(x, y);
			else if(bird2_current == 2)
				sprs.get(SprBird2_3).setPosition(x, y);
			// End of the animation
			if(x >= TopLeftToMidCoordX(StandarValues.SCREEN_WIDTH))
			{	// resets the animation's sprites
				bird2 = false;
				sprs.get(SprBird2_1).setPosition(TopLeftToMidCoordX(-16f), TopLeftToMidCoordY(292+(rand%105)));
				// makes them invisible
				sprs.get(SprBird2_1).setColor(1,1,1,0);
				sprs.get(SprBird2_2).setColor(1,1,1,0);
				sprs.get(SprBird2_3).setColor(1,1,1,0);
			}
		}
		else
		{
			if((rand >= -2666343) && (rand <= 6598343))
				bird2 = true;
		}
	}
	/** Loads the bee effect animation */
	public void EffectBee(int rand, float delta)
	{
		if(bee) // the animation has not finished yet
		{
			float x=sprs.get(SprBee1_1).getX();
			float y=sprs.get(SprBee1_1).getY();
			if((x<=TopLeftToMidCoordX(263) && x>=TopLeftToMidCoordX(168)) && 
					(y<=TopLeftToMidCoordY(251) && y>=TopLeftToMidCoordY(310)) && !bee_to_leave_flower)// Checks if the bee is near a flower
			{	// kinds of stop the bee to make it seem that it is in the flower
				// here the bee should follow a route right to the flower
				int rs[]=GetCoordRouteType1((int)x, (int)y, TopLeftToMidCoordX(207), TopLeftToMidCoordY(298));
				if(rs[0]==x) // reached the flower now slowly starts time to leave it
					animation_bee_change_time -= delta;
				x = rs[0]; 
				y = rs[1];
				sprs.get(SprBee1_1).setPosition(x, y);
				if(animation_bee_change_time<=0)
				{
					bee_to_leave_flower = true;
					animation_bee_change_time = 27.4f+(rand%17f);
				}
			}
			else
			{	// the bee follows its common route
				x -= 2.16;
				int aux = (int)x + (rand%19);
				y += MathUtils.sin(aux/12)*2; //costly operation
				sprs.get(SprBee1_1).setPosition(x, y);
			}
			
			// End of the animation
			if(x+11 <= TopLeftToMidCoordX(0))	// out of the camera view
			{	// resets the animation's sprites
				bee = false;
				bee_to_leave_flower = false;
				sprs.get(SprBee1_1).setPosition(StandarValues.SCREEN_WIDTH+11 , TopLeftToMidCoordY(286+rand%47));
				// make invisible
				sprs.get(SprBee1_1).setColor(1,1,1,0);
			}
		}
		else
		{
			if((rand >= 564) && (rand <= 6990923))
			{
				bee = true;
				sprs.get(SprBee1_1).setColor(1,1,1,1);
			}
		}
	}
	// ----
	/** returns if the point is inside a rectangle, 
	 * Note.- the rectangle is meant to be from the top left corner*/
	public boolean InsideRectangle(int x, int y, int posrx, int posry, int width, int height)
	{
		if((x>=posrx && x<=posrx+width) && (y<=posry+height && y>=posry))
			return true;
		return false;
	}

	/** Returns the x value relative to the game, adapting in case of bigger or
	 * smaller screen sizes*/
	public float GetRelativeX()
	{
		float coorx ;
		if(Gdx.app.getType() == ApplicationType.Android)
		{
			coorx = (Gdx.input.getX()*100)/Gdx.graphics.getWidth();
			coorx/=100;
			coorx *= StandarValues.SCREEN_WIDTH;
		}
		else
			coorx = Gdx.input.getX();
		return coorx;
	}
	/** Returns the y value relative to the game, adapting in case of bigger or
	 * smaller screen sizes*/
	public float GetRealitveY()
	{
		float coory;
		if(Gdx.app.getType() == ApplicationType.Android)
		{
			coory = (Gdx.input.getY()*100)/Gdx.graphics.getHeight();
			coory/=100;
			coory *= StandarValues.SCREEN_HEIGHT;
		}
		else
			coory = Gdx.input.getY();
		return coory;
	}
	/** Transforms the value of X axis from coordinate system with origin top left corner
	 * to the used in sprite coordinates system with origin at the center*/
	public int TopLeftToMidCoordX(float x)
	{
		return (int)(x-StandarValues.VIEWPORT_WIDTH_MID);
	}
	/** Transforms the value of Y axis from coordinate system with origin top left corner
	 * to the used in sprite coordinates system with origin at the center*/
	public int TopLeftToMidCoordY(float y)
	{
		return (int)(StandarValues.VIEWPORT_HEIGHT_MID-y);
	}
	/** Returns a destiny point that is 1 unit closer to a given source point*/
	private int[] GetCoordRouteType1(int srcX, int srcY, int destX, int destY)
	{
		int vals[] = {srcX, srcY};
		
		if(srcX > destX)
			vals[0]-=1;
		else if(srcX < destX)
			vals[0]+=1;
		
		if(srcY > destY)
			vals[1]-=1;
		else if(srcY < destY)
			vals[1]+=1;
		
		return vals;
	}
	
	public boolean visible() { return this.visible; }
}