/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.tuxfamily.chwnku.io.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Circle extends Collidable{
	
	protected Point2d Center;
	protected int Radius;
	protected Texture texture;
	protected Color BodyColor;
	protected Color LineColor;
	
	/** The basic constructor of the circle */
	public Circle (int radius, Point2d center) 
	{
		this.Radius = radius;
		this.Center = center;
	}

	/** Given a point checks if it collides or is inside the area of the collidable object */
	public boolean Collides (Point2d point) 
	{
		return false;
	}

	/** Given a polygon checks if it collides or is inside the area of the collidable object */
	public  boolean Collides (Polygon polygon) 
	{
		return false;
	}

	/** Given a circle checks if it collides or is inside the area of the collidable object */
	public void Collides (Circle circle)
	{
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void SetVisible(boolean visible) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw(SpriteBatch batch) {
		// TODO Auto-generated method stub
		
	}
}
