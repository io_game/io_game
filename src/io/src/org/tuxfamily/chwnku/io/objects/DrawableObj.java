/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.tuxfamily.chwnku.io.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/** The basic abstraction of an object that can be drawn on the screen*/
public abstract class DrawableObj {
	
	protected boolean visible;
	protected String ID;
	protected float scaleX;
	protected float scaleY;

	public DrawableObj() {
		
	}

	/** Uses appropriated methods to draw the object according to its attributes  */
	public abstract void draw(SpriteBatch batch);

	/** Cleans the memory that the object was using so it is utterly eliminated */
	public abstract void dispose ();

	/** Updates the object's state */
	public abstract void update ();

	/** Sets the object's visibility, if visible is true it is going to be drawn*/
	public abstract void SetVisible (boolean visible);

}
