/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.tuxfamily.chwnku.io.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

/*Contains the basic methods that a collidable object must implement*/
public abstract class Collidable extends DrawableObj{

	protected Array<Integer> tiles;
	
	@Override
	public abstract void draw(SpriteBatch batch);

	@Override
	public abstract void dispose() ;

	@Override
	public abstract void update() ;

	@Override
	public abstract void SetVisible(boolean visible);
	
	/** Given a point checks if it collides or is inside the area of the collidable object */
	public abstract boolean Collides (Point2d point);

	/** Given a polygon checks if it collides or is inside the area of the collidable object */
	//public abstract boolean Collides (Polygon polygon);

	/** Given a circle checks if it collides or is inside the area of the collidable object */
	//public abstract void Collides (Circle circle);

}
