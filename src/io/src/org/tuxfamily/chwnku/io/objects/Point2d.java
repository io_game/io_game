/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.objects;

/** A basic implementation of the geometric two dimensional point
 */
public class Point2d {

	private float x,y;
	
	public Point2d() {};
	
	public Point2d(float x, float y)
	{
		this.x = x; this.y = y;
	}
	
	/** Returns the distance from a given point*/
	public float distance(float x, float y)
	{	
		float X = x - this.x;
		float Y = y - this.y;
		return (float) (Math.sqrt((X*X)+(Y*Y))); // see if there is a faster method than Math.sqrt
	}
	/** Returns the distance from a given point*/
	public float distance(Point2d p)
	{
		return (float)(Math.sqrt(((p.getX()-this.x)*(p.getX()-this.x)) + ((p.getY()-this.y)*(p.getY()-this.y))));
	}
	
	public void move(float x, float y)
	{
		this.y = y;
		this.x = x;
	}
	
	
	public float getX() { return this.x;	}
	public void setX(float x) {	this.x = x;	}
	public float getY() { return this.y;	}
	public void setY(float y) {	this.y = y;	}
	
}
