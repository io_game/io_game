/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.objects;

/** A basic implementation of a 2d segment line*/
public class Segment2d {

	private Point2d p0,p1;
	
	public Segment2d()
	{
		p0 = new Point2d();
		p1 = new Point2d();
	}
	
	public Segment2d(Point2d p0, Point2d p1)
	{
		this.p0 = p0;
		this.p1 = p1;
	}
	
	public Segment2d(float x0, float y0, float x1, float y1)
	{
		this.p0 = new Point2d(x0, y0);
		this.p1 = new Point2d(x1, y1);
	}
	/** Returns the angle of the line segment (0 to 360) clockwise*/ 
	public float angle()
	{
		// TODO: verify implementation and see a faster way (do not use double)
		return (float) (Math.tan((this.p1.getY()-this.p0.getY())/(this.p1.getX()-this.p0.getX())));
	}
	
	/** Rotates the segment the given angle<p> Angle is from 0 to 360 clockwise*/
	public void rotate(int angle)
	{
		
	}
	
	/** Rotates the segment the given angle<p> Angle is from 0 to 360 counter-clockwise*/
	public void rotateCcw(int angle)
	{
		
	}
	/** Moves the segment to the specified point*/
	public void move(float x, float y)
	{
		
	}
	
	public Point2d getP0() {		return p0;	}
	public void setP0(Point2d p0) {		this.p0 = p0;	}
	public Point2d getP1() {		return p1;	}
	public void setP1(Point2d p1) {		this.p1 = p1;	}
	
}
