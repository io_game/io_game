/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.objects;

import org.tuxfamily.chwnku.io.utilities.Ut;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class Polygon extends Collidable{
	
	protected Array<Point2d> Vertex;
	protected int Nvertex;
	protected Array<Segment2d> Segments;
	//protected Texture texture;
	protected Color BodyColor;
	protected Color LineColor;
	
	private Array<Sprite> sprites = new Array<Sprite>();
	
	public Polygon()
	{
		this.BodyColor = new Color(0.52f, 0.48f, 0.17f, 1);
		this.LineColor = new Color(0.11f, 0.11f, 	0.09f, 1);
		this.visible = false;
	}
	
	public Polygon(Color bodycol, Color linecol)
	{
		this.BodyColor = bodycol;
		this.LineColor = linecol;
	}
	
	/** From a point given this method evaluates if it is a valid vertex and if so, 
	 * <p> it is added as an additional vertex of the polygon */
	public void AddVertex (Point2d point) 
	{
		Sprite aux =  new Sprite();
		if(Vertex.size == 0) // first point
		{
			this.visible = true;
			Nvertex = 1;
			Vertex.add(point);
			sprites.add(Ut.GetCicularSprite(2, Vertex.peek(), this.LineColor));
		}
	}
	
	/** Checks if the polygon is valid, thus if no line segments intersects nor it collides with other objects */
	public boolean Check () 
	{
		return false;
	}
	
	@Override
	public void draw(SpriteBatch batch) 
	{
		if(this.visible)
		{
			for(Sprite spr: sprites)
			{
				spr.draw(batch);
			}
		}
	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void SetVisible(boolean visible) 
	{
		// TODO Auto-generated method stub
	}

	@Override
	public boolean Collides(Point2d point) 
	{
		// TODO Auto-generated method stub
		return false;
	}

}
