/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.utilities;


public class StandarValues {
	
	// -- general screen adjustment --
	public static final float SCREEN_WIDTH = 800.0f;
	public static final float SCREEN_HEIGHT = 480.0f;
	// to define the game world as 800 x 480 meters !
	public static final float VIEWPORT_WIDTH = 800f;
	public static final float VIEWPORT_HEIGHT = 480f;
	public static final float VIEWPORT_WIDTH_MID = VIEWPORT_WIDTH/2;
	public static final float VIEWPORT_HEIGHT_MID = VIEWPORT_HEIGHT/2;
	// ----
	// Files for preferences and language
	public static final String LANGUAGES_LIST_FILE = "data/langs/languages_list";
	public static final String PREFERENCES_FILE = "io_preferences";
	// Assets for menu screen and level editor
	//public static final String MENU_SCREEN_BACKGROUND = "../../images/io_menu.png";
	public static final String TEXTURE_ATLAS_MENU_SCREEN = "images/io_basic.pack";
	public static final String TEXTURE_ATLAS_ANIMATE = "images/io_animate.pack"; 
	
	// LevelEditor settings
	public static final float LE_MSG_WIDTH = 694.0f;
	
	// Screens code numbers
	public static final byte N_MAIN_MENU_SCREEN = 0;
	public static final byte N_HELP_SCREEN = 1;
	public static final byte N_LEVEL_EDITOR_SCREEN = 2;
	public static final byte N_PLAY_SCREEN = 3;
	
	// Level editor interface code numbers
	// SEE: "Docu/leditor/interface_current_state"
	// -- Code numbers --
	public static final int N_SELECTION = 1;
	public static final int File_M = 100;
	public static final int File_M_new = 101;
	public static final int Selection_b_ns = 200;
	public static final int Selection_b_os = 201;
	public static final int Delete_b_ns = 300;
	public static final int Delete_b_os = 301;
	public static final int Duplicate_b_ns = 400;
	public static final int Duplicate_b_os = 401;
	public static final int Rotate_b_ns = 500;
	public static final int Rotate_b_os = 501;
	public static final int Move_b_ns = 600;
	public static final int Move_b_os = 601;
	public static final int Polygon_b_np = 700;
	public static final int Polygon_b_1p = 701;
	public static final int Polygon_b_2p = 702;
	public static final int Polygon_b_mp = 703;
	public static final int Complements_b_w1 = 800;
	public static final int Complements_b_w2 = 801;
	public static final int Powerups_b_w1 = 900;
	public static final int Powerups_b_w2 = 901;
	public static final int Powerups_b_w3 = 902;
	public static final int Powerups_b_os = 903;
}
