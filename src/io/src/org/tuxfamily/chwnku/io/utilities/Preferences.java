/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.utilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class Preferences implements java.io.Serializable{
	
	public static String TAG = Preferences.class.getName();
	
	public static Preferences instance = new Preferences();
	
	public String Language;
	public int SoundVolume;
	public int MusicVolume;
	public boolean FPS;
	
	// singleton prevents initialization from outside
	private Preferences() { }
	
	/** Loads the preferences from preferences file and if does not exists it is created
	 * with default values
	 * @throws IOException 
	 * @throws FileNotFoundException */
	public void LoadPreferencesFile() throws IOException, ClassNotFoundException
	{
		FileHandle file = Gdx.files.local(StandarValues.PREFERENCES_FILE);
		if(file.exists())
		{
			Gdx.app.log(TAG, "Reading preferences file...");
			instance = (Preferences) deserialize(file.readBytes());
			Gdx.app.log("language ", instance.Language);
		}
		else
		{
			Gdx.app.log(TAG, "Creating preferences file...");
			OutputStream out = null;
			try
			{
				file.file().createNewFile();
				// -- default values --
				instance.Language = "en_US";
				instance.SoundVolume = 80;
				instance.MusicVolume = 75;
				instance.FPS = false;
				// writing to binary file
				file.writeBytes(serialize(instance), false);
				// ----
			} catch (IOException e)
			{
				Gdx.app.log(TAG, e.toString());
				 e.printStackTrace();
			} finally{
		        if(out != null) try{out.close();} catch(Exception ex){}
		    }
		}
	}

	public static byte[] serialize(Object obj) throws IOException
	{
	    ByteArrayOutputStream b = new ByteArrayOutputStream();
	    ObjectOutputStream o = new ObjectOutputStream(b);
	    o.writeObject(obj);
	    return b.toByteArray();
	}
	
	public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException 
	{
	    ByteArrayInputStream b = new ByteArrayInputStream(bytes);
	    ObjectInputStream o = new ObjectInputStream(b);
	    return o.readObject();

	}
	
	public class MiObjectOutputStream extends ObjectOutputStream	{
		 /** Constructor que recibe OutputStream */
	    public MiObjectOutputStream(OutputStream out) throws IOException
	    {
	        super(out);
	    }

	    /** Constructor sin parámetros */
	    protected MiObjectOutputStream() throws IOException, SecurityException
	    {
	        super();
	    }

	    /** Redefinición del método de escribir la cabecera para que no haga nada. */
	    protected void writeStreamHeader() throws IOException
	    {	    }
	}
	
}