/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.utilities;

import org.tuxfamily.chwnku.io.objects.Point2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Array;


/** Utilities class, some useful methods can be called from here */
public class Ut {
	public static final String TAG = Ut.class.getName();
	
	public static final Ut ut = new Ut();
	
	// singleton
	private Ut() { }
	
	// -- rectangles --
	/** returns if the point is inside a rectangle, 
	 * <p>
	 * Note.- the rectangle is meant to be from the top left corner*/
	public static boolean InsideRectangle(int x, int y, int posrx, int posry, int width, int height)
	{
		if((x>=posrx && x<=posrx+width) && (y<=posry && y>=posry-height))
			return true;
		return false;
	}
	
	/*
	 public static boolean InsideRectangle(int x, int y, int posrx, int posry, int width, int height)
	{
		if((x>=posrx && x<=posrx+width) && (y>=posry && y<=posry+height))
			return true;
		return false;
	} 
	 */
	// ----
	// -- input --
	/** Returns the x input value relative to the game, adapting in case of bigger or
	 * smaller screen sizes, useful when requesting for input event*/
	public static float GetRelativeX()
	{
		float coorx ;
		if(Gdx.app.getType() == ApplicationType.Android)
		{
			coorx = (Gdx.input.getX()*100)/Gdx.graphics.getWidth();
			coorx/=100;
			coorx *= StandarValues.VIEWPORT_WIDTH;
		}
		else
			coorx = Gdx.input.getX();
		return coorx;
	}
	/** Returns the y input value relative to the game, adapting in case of bigger or
	 * smaller screen sizes, useful when requesting for input event*/
	public static float GetRealitveY()
	{
		float coory;
		if(Gdx.app.getType() == ApplicationType.Android)
		{
			coory = (Gdx.input.getY()*100)/Gdx.graphics.getHeight();
			coory/=100;
			coory *= StandarValues.VIEWPORT_HEIGHT;
		}
		else
			coory = Gdx.input.getY();
		return coory;
	}
	// ----
	// -- coordinate system and angles --
	/** Transforms the value of X axis from coordinate system with origin top left corner
	 * to the used in sprite coordinates system with origin at the center*/
	public static int TopLeftToMidCoordX(float x)
	{
		return (int)(x-StandarValues.VIEWPORT_WIDTH_MID);
	}
	/** Transforms the value of Y axis from coordinate system with origin top left corner
	 * to the used in sprite coordinates system with origin at the center*/
	public static int TopLeftToMidCoordY(float y)
	{
		return (int)(StandarValues.VIEWPORT_HEIGHT_MID-y);
	}
	
	public static float RadToGrad(float rad)
	{
		return (float) ((180*rad)/Math.PI);
	}
	
	// ----
	
	// -- useful point operations --
	/** Returns a destiny point that is 1 unit closer to a given source point*/
	private static int[] GetCoordRouteType1(int srcX, int srcY, int destX, int destY)
	{
		int vals[] = {srcX, srcY};
		
		if(srcX > destX)
			vals[0]-=1;
		else if(srcX < destX)
			vals[0]+=1;
		
		if(srcY > destY)
			vals[1]-=1;
		else if(srcY < destY)
			vals[1]+=1;
		
		return vals;
	}
	// ----
	
	// -- text utils --
	/** Returns the total width (pixels) that a given text takes referencing the given font
	 * <p>
	 * Note.-It is used the getBounds method of libgdx's class TextBounds */
	public static int GetTextWidth(String text, BitmapFont font)
	{
		TextBounds bounds = new TextBounds();
		bounds = font.getBounds(text);
		return (int)bounds.width;
	}
	/** Returns an Array<String> containing the split text to fit in a given width, of the given font
	 * <p> Note.- Words are not divided*/
	public static Array<String> SplitText(String text, BitmapFont font, float width)
	{
		Array<String> array = new Array<String>();
		String[] words = text.split(" ");
		String aux = "";
		String last = "";
		for(int i=0 ; i<words.length ; i++)
		{
			last = aux + ' ';
			aux += words[i] + ' ';
			if(GetTextWidth(aux, font) > width)
			{
				array.add(last);
				aux = words[i] + ' ';
			}
		}
		array.add(aux);

		return array;
	}
	/** Returns an Array<String> containing the split text to fit in a given width, of the given font
	 * <p>Note.- Words are divided if they don't fit */
	public static Array<String> SplitTextStrict(String text, BitmapFont font, float width)
	{ // TODO: The method does not work correctly yet manage the space character!
		Array<String> array = new Array<String>();
		int chn = 0;
		float length = 0;
		String aux = "";
		for(chn=0; chn < text.length(); chn++)
		{
			aux += text.charAt(chn);
			length += GetTextWidth(chn+"", font);
			if(length > width)
			{
				array.add(aux);
				aux="";
				length = 0;
			}
		}
		array.add(aux);
		
		Gdx.app.log(TAG, array.toString());
		return array;
	}	
	// ----
	
	// -- Some sprites functions --
	/** Returns a circular sprite given the radius, the point and RGBA color*/
	public static Sprite GetCicularSprite(int radius, Point2d point, Color color)
	{
		Pixmap pixmap = new Pixmap(8, 8, Format.RGBA8888);
		pixmap.setColor(color.r,color.g,color.b,color.a);
		pixmap.fillCircle((int)(pixmap.getWidth()/2), (int)(pixmap.getHeight()/2), radius);
		Texture texture = new Texture(pixmap);
		Sprite spr = new Sprite(texture);
		spr.setPosition(point.getX()-radius, point.getY()-radius);
		spr.setSize(radius+radius, radius+radius);
		pixmap.dispose();
		return spr;
	}
	
	// ----
}
