/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.tuxfamily.chwnku.io.utilities;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

public class LanguageText {
	public static final String TAG = LanguageText.class.getName();
	
	public static final LanguageText instance = new LanguageText();
	public String Name="English"; // current's language name
	public String CodeName="en_US"; // current's language codename
	private Array<String> Languages_names;	 // list of
	private Array<String> Languages_codenames;
	// Menu Screen
	public String MainMenu_PlayButton;
	public String MainMenu_LevelEditorButton;
	public String MainMenu_OptionsButton;
	public String MainMenu_QuitButton;
	public String MainMenu_HelpButton;
	public String MainMenu_LanguageButton;
	public String MainMenu_AboutButton;
	// Level Editor
	public String Leditor_WelcomeText;
	public String Leditor_FileBtn;
	public String Leditor_SelectionBtn;
	public String Leditor_DeleteBtn;
	public String Leditor_DuplicateBtn;
	public String Leditor_RotateBtn;
	public String Leditor_MoveBtn;
	public String Leditor_PolygonBtn;
	public String Leditor_ComplementBtn;
	public String Leditor_PowerupBtn;
	public String Leditor_ChangeScreenBtn;
	public String Leditor_PropertiesBtn;
	
	// singleton to prevent initialization from other 
	private LanguageText()	{		}

	/** initializate the Language*/
	public void init()
	{
		LoadPreferences();
	}
	public void LoadPreferences()
	{
		try {
			Preferences.instance.LoadPreferencesFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LoadLanguage(Preferences.instance.Language);		
	}
	/** Loads a language from a given translation filename*/
	public void LoadLanguage(String language)
	{
		 //the file must be in andoroid's folder assets
		if (Gdx.files.internal(StandarValues.LANGUAGES_LIST_FILE).exists())
		{
			// getting valid content of the language_list file
			this.Languages_names = GetLangNames();
			this.Languages_codenames = GetLangCodeNames();
			if(ArrayContains(language, this.Languages_codenames))
			{	// then loads the text from translation file
				Gdx.app.log(TAG, " loading file:"+language);
				LoadTextFromLanguageFile(language);
			}
			else	
			{	// here should load default text in case of fail
				Gdx.app.log(TAG, "ERROR finding "+language+" file, loading default (en_US)");
				LoadTextFromLanguageFile("en_US");
			}
		}
		else	
		{ 	//here code to load default language should above operations fail
			//Menu Screen
			MainMenu_PlayButton = "PLAY";
			MainMenu_LevelEditorButton = "LEVEL EDITOR";
			MainMenu_OptionsButton = "OPTIONS";
			MainMenu_QuitButton = "Quit";
			MainMenu_HelpButton = "Help";
			MainMenu_LanguageButton = "Language ("+this.Name+")";
			MainMenu_AboutButton = "About";
			// level editor
			Leditor_FileBtn = "File menu";
			Leditor_WelcomeText = "Welcome :), go to File>Help for more info about the editor, what lesson will you include?";
			Leditor_SelectionBtn = "Select an object";
			Leditor_DeleteBtn = "Delete an object";
			Leditor_DuplicateBtn = "Duplicate an object";
			Leditor_RotateBtn = "Rotate an object";
			Leditor_MoveBtn = "Move an object";
			Leditor_PolygonBtn = "Create a polygon";
			Leditor_ComplementBtn = "Put a complement";
			Leditor_PowerupBtn = "Put a powerup";
			Leditor_ChangeScreenBtn = "Change to another screen";
			Leditor_PropertiesBtn = "Obeject's properties";
		}
	}
	/** iterating trough all available languages loads the next language*/
	public void ChangeToNextLanguage()
	{
		int l = Languages_codenames.size;
		String lang;
		for(int i=0 ; i<l ; i++)
		{
			lang = Languages_codenames.get(i);
			if(lang.equals(this.CodeName))
			{
				if(i+1 == l) // changes to the next language
					this.CodeName = Languages_codenames.get(0);
				else
					this.CodeName = Languages_codenames.get(i+1);
				break;
			}
		}
		LoadLanguage(this.CodeName);
	}
	
	//  -- utilities for reading language files --
	/** Returns an Array<String> where each String contains all valid lines in the file*/
	public Array<String> GetValidContent(String fileName)
	{
		if(!fileName.equals(StandarValues.LANGUAGES_LIST_FILE))
			fileName = "data/langs/"+fileName; // updating folder
		Array<String> content = new Array<String>();
		FileHandle file = Gdx.files.internal(fileName);
		String allfile = file.readString();
		Gdx.app.log(TAG, "reading file:"+fileName);
		
		String lines[] = allfile.split("\n");
		
		for(String line: lines)
		{
			if(!line.startsWith("#") && line.length()>0)
				content.add(line);
		}
		//Gdx.app.log(TAG, "Valid Content="+content.toString());
		return content;
	}
	/** Returns an Array<String> with all the languages names implemented*/
	public Array<String> GetLangNames()
	{
		Array<String> lines = GetValidContent(StandarValues.LANGUAGES_LIST_FILE);
		Array<String> lang_names= new Array<String>();
		for(String line: lines)
		{
			if(line.length()>0)
			{	// adds the name and gets rid off the two " 
				String aux[] = line.split(" ");
				lang_names.add(aux[2].replace("\"", ""));
			}
		}
		return lang_names;
	}
	/** Returns an Array<String> with all the languages codenames implemented*/
	public Array<String> GetLangCodeNames()
	{
		Array<String> lines = GetValidContent(StandarValues.LANGUAGES_LIST_FILE);
		Array<String> lang_codenames= new Array<String>();
		for(String line: lines)
		{	// adds the codename and gets rid off the two " 
			String aux[] = line.split(" ");
			lang_codenames.add(aux[1].replace("\"", ""));
		}
		return lang_codenames;
	}
	/** Fill the variables with the translated text from a language file */
	public void LoadTextFromLanguageFile(String filename)
	{
		Array<String> content = GetValidContent(filename);
		this.CodeName = filename;
		this.Name = this.Languages_names.get(ArrayIndexOf(filename, Languages_codenames));
		// *) Note that the searched string has the same name as the variable
		// MainMenuScreen
		MainMenu_PlayButton = GetTranslatedText("MainMenu_PlayButton", content);
		MainMenu_LevelEditorButton = GetTranslatedText("MainMenu_LevelEditorButton", content);
		MainMenu_OptionsButton = GetTranslatedText("MainMenu_OptionsButton", content);
		MainMenu_QuitButton = GetTranslatedText("MainMenu_QuitButton", content);
		MainMenu_HelpButton = GetTranslatedText("MainMenu_HelpButton", content);
		MainMenu_LanguageButton = GetTranslatedText("MainMenu_LanguageButton", content);
		MainMenu_AboutButton = GetTranslatedText("MainMenu_AboutButton", content);
		// Level Editor
		Leditor_FileBtn = GetTranslatedText("Leditor_FileBtn", content);
		Leditor_WelcomeText = GetTranslatedText("Leditor_WelcomeText", content);
		Leditor_SelectionBtn = GetTranslatedText("Leditor_SelectionBtn", content);
		Leditor_DeleteBtn = GetTranslatedText("Leditor_DeleteBtn", content);
		Leditor_DuplicateBtn = GetTranslatedText("Leditor_DuplicateBtn", content);
		Leditor_RotateBtn = GetTranslatedText("Leditor_RotateBtn", content);
		Leditor_MoveBtn = GetTranslatedText("Leditor_MoveBtn", content);
		Leditor_PolygonBtn = GetTranslatedText("Leditor_PolygonBtn", content);
		Leditor_ComplementBtn = GetTranslatedText("Leditor_ComplementBtn", content);
		Leditor_PowerupBtn = GetTranslatedText("Leditor_PowerupBtn", content);
		Leditor_ChangeScreenBtn = GetTranslatedText("Leditor_ChangeScreenBtn", content);
		Leditor_PropertiesBtn = GetTranslatedText("Leditor_PropertiesBtn", content);
	}
	/** From a original text string returns the translated version of that string */
	public String GetTranslatedText(String original_text, Array<String> content)
	{
		String translated_text="- missing -";
		for(String line : content)
		{	// Uses the substring "-=-" as divider, so the last part will be the translated text
			// There must be necessary a new method to detect the translated text from translation files
			// Also it is necessary to include new lines in a translated text! 
			// TODO: upgrade the format of translated files
			String aux[] = line.split("\"-=-\"");
			//Gdx.app.log("GettingTranslatedCODE", aux[0]+" "+aux[1]);
			if(aux[0].substring(0, aux[0].indexOf(' ')).equals(original_text))
			{
				translated_text = aux[1].replace("\"","");
				break;
			}
		}
		return translated_text;
	}
	public boolean ArrayContains(String StrToFind, Array<String> array)
	{
		for(String str: array)
		{
			if(str.equals(StrToFind))
				return true;
		}
		return false;
	}
	public int ArrayIndexOf(String StrToFind, Array<String> array)
	{
		int i = 0;
		for(String str: array)
		{
			if(str.equals(StrToFind))
				return i;
			i++;
		}
		return -1;
	}
	// ----

}
