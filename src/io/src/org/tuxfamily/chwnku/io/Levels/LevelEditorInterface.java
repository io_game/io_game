/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.Levels;

import org.tuxfamily.chwnku.io.game.Assets;
import org.tuxfamily.chwnku.io.utilities.LanguageText;
import org.tuxfamily.chwnku.io.utilities.StandarValues;
import org.tuxfamily.chwnku.io.utilities.Ut;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/** Contains the methods that implement level editor interface and functionalities*/
public class LevelEditorInterface {
	public static final String TAG = LevelEditorInterface.class.getName();
	
	protected int SelectedItem;
	public Level LoadedLevel;
	public String LoadedLevelPath;

	private Array<Sprite> sprites_nm = new Array<Sprite>();
	private Array<Sprite> sprites_effects = new Array<Sprite>();
	
	private Array<String> MessageArea;
	private BitmapFont fnt = Assets.instance.fonts.defaultNormal;
	private int msg_page, b_arrow_index, f_arrow_index, arrow_e_offset;
	private int sprsnumber;
	
	private int c_interface;
	
	public LevelEditorInterface()
	{
		c_interface = StandarValues.N_SELECTION;
		LoadedLevel = null;
		LoadedLevelPath = "";
		this.MessageArea = new Array<String>();
		this.fnt.setColor(0.9f,0.90f,0.97f,1f);
		this.fnt.setScale(1.62f, 1.5f);	// the text is a little wider to improve reading on small screens
		this.msg_page = 0; this.arrow_e_offset = 0;
	}

	public void LoadLevel (String filepath)
	{
		// TODO: Add the level loading from filePath
		Vector2 vect = new Vector2();
	}

	public void LoadAssets()
	{
		this.MessageArea = Ut.SplitText(LanguageText.instance.Leditor_WelcomeText, fnt, StandarValues.LE_MSG_WIDTH);
		sprsnumber = 0;
		Array<TextureRegion> regions = new Array<TextureRegion>();
		regions.add(Assets.instance.leditor_panels.left_panel);
		regions.add(Assets.instance.leditor_panels.superior_panel);
		regions.add(Assets.instance.leditor_icons.selection);
		regions.add(Assets.instance.leditor_icons.delete);
		regions.add(Assets.instance.leditor_icons.duplicate);
		regions.add(Assets.instance.leditor_icons.rotate);
		regions.add(Assets.instance.leditor_icons.move);
		regions.add(Assets.instance.leditor_icons.polygon);
		regions.add(Assets.instance.leditor_icons.complements);
		regions.add(Assets.instance.leditor_icons.powerups);
		regions.add(Assets.instance.leditor_icons.file);
		regions.add(Assets.instance.leditor_icons.change_screen);
		regions.add(Assets.instance.leditor_icons.properties);
		regions.add(Assets.instance.leditor_icons.b_arrow_t);
		regions.add(Assets.instance.leditor_icons.f_arrow_t);
		// panels background
		Pixmap pixmap = new Pixmap(1, 1,Format.RGBA8888);
		pixmap.setColor(0x2C/255f, 0x7C/255f, 0x4D/255f, 0.86f);
		pixmap.fill();
		Texture texture = new Texture(pixmap);
		Sprite spr = new Sprite(texture);
		// left panel bg
		sprites_nm.add(spr); sprsnumber++;
		sprites_nm.peek().setSize(50f,StandarValues.VIEWPORT_HEIGHT);
		sprites_nm.peek().setPosition(Ut.TopLeftToMidCoordX(0f), Ut.TopLeftToMidCoordY(StandarValues.VIEWPORT_HEIGHT));
		// superior panel bg
		spr = new Sprite(texture);
		sprites_nm.add(spr); sprsnumber++;
		sprites_nm.peek().setSize(StandarValues.VIEWPORT_WIDTH,50f);
		sprites_nm.peek().setPosition(Ut.TopLeftToMidCoordX(0f), Ut.TopLeftToMidCoordY(50f));
		// left panel
		spr = new Sprite(regions.get(0));
		spr.setSize(50f, 430f);
		spr.setPosition(Ut.TopLeftToMidCoordX(0f), Ut.TopLeftToMidCoordY(StandarValues.SCREEN_HEIGHT));
		sprites_nm.add(spr); sprsnumber++;
		// superior panel
		spr = new Sprite(regions.get(1));
		spr.setSize(800f, 50f);
		spr.setPosition(Ut.TopLeftToMidCoordX(0f), Ut.TopLeftToMidCoordY(50f));
		sprites_nm.add(spr); sprsnumber++;
		// selection button
		spr = new Sprite(regions.get(2));
		spr.setSize(48f, 48f); 
		spr.setPosition(Ut.TopLeftToMidCoordX(1f), Ut.TopLeftToMidCoordY(102));
		sprites_nm.add(spr); sprsnumber++;
		// delete button
		spr = new Sprite(regions.get(3));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(1f), Ut.TopLeftToMidCoordY(154));
		sprites_nm.add(spr); sprsnumber++;
		// duplicate button
		spr = new Sprite(regions.get(4));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(1f), Ut.TopLeftToMidCoordY(206f));
		sprites_nm.add(spr); sprsnumber++;
		// rotate button
		spr = new Sprite(regions.get(5));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(1f), Ut.TopLeftToMidCoordY(258f));
		sprites_nm.add(spr); sprsnumber++;
		// move button
		spr = new Sprite(regions.get(6));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(1f), Ut.TopLeftToMidCoordY(310f));
		sprites_nm.add(spr); sprsnumber++;
		// polygon button
		spr = new Sprite(regions.get(7));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(1f), Ut.TopLeftToMidCoordY(362f));
		sprites_nm.add(spr); sprsnumber++;
		// complements button
		spr = new Sprite(regions.get(8));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(1f), Ut.TopLeftToMidCoordY(414f));
		sprites_nm.add(spr); sprsnumber++;
		// powerups button
		spr = new Sprite(regions.get(9));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(1f), Ut.TopLeftToMidCoordY(466f));
		sprites_nm.add(spr); sprsnumber++;
		// file button
		spr =  new Sprite(regions.get(10));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(1f), Ut.TopLeftToMidCoordY(50f));
		sprites_nm.add(spr); sprsnumber++;
		// change screen button
		spr = new Sprite(regions.get(11));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(750f), Ut.TopLeftToMidCoordY(50f));
		sprites_nm.add(spr); sprsnumber++;
		// properties button
		spr = new Sprite(regions.get(12));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(750f), Ut.TopLeftToMidCoordY(50f));
		spr.setColor(1f,1f,1f,0f);
		sprites_nm.add(spr); sprsnumber++;
		// b_arrow_t
		spr = new Sprite(regions.get(13));
		spr.setSize(24f, 18f);
		spr.setPosition(Ut.TopLeftToMidCoordX(60f), Ut.TopLeftToMidCoordY(30f));
		spr.setColor(1f,1f,1f,0f);
		sprites_nm.add(spr); this.b_arrow_index = sprsnumber++;
		// f_arrow_t
		spr = new Sprite(regions.get(14));
		spr.setSize(24f, 18f);
		spr.setPosition(Ut.TopLeftToMidCoordX(700f), Ut.TopLeftToMidCoordY(30f));
		spr.setColor(1f,1f,1f,0f);
		sprites_nm.add(spr); this.f_arrow_index = sprsnumber++;
		
		// dummy effects (they are 100 translucent, in render method are now drawn)
		pixmap.setColor(1f,1f,1f,0f);
		texture =  new Texture(pixmap);
		spr = new Sprite(texture);
		
		sprites_effects.add(spr);	
		sprites_effects.add(spr);
		sprites_effects.add(spr);
		sprites_effects.add(spr);
		/*
		spr = new Sprite(regions.get(12));
		spr.setSize(48f, 48f);
		spr.setPosition(Ut.TopLeftToMidCoordX(750f), Ut.TopLeftToMidCoordY(1f));
		sprites_nm.add(spr); sprsnumber++;
		 */
	}
	
	// effects index:
	private final int e_btnpressed = 0;	// button pressed
	private final int e_hlpressed = 1;	// highlight pressed 
	private final int e_btnselected = 2;// button selected
	private final int e_hlselected = 3;	// highlight selected
	
	// -- Input Handler --
	
	/** Test for the button or object *selected */
	public void TestSelected(int X, int Y)
	{
		if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(50), Ut.TopLeftToMidCoordY(50), 750, 430))
		{	// Inside edition area
			
		}
		else
		{	// Inside panels
			if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(51), 49, 49))
			{	// selection button selected
				ChangeMessageArea(LanguageText.instance.Leditor_SelectionBtn);
				sprites_effects.set(e_btnselected, 
						RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(101), 50, 50, 0.28f, 0.9f, 0.1f, 0.44f));
			}
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(103), 49, 49))
			{	// delete button selected
				ChangeMessageArea(LanguageText.instance.Leditor_DeleteBtn);
				sprites_effects.set(e_btnselected, 
						RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(154), 50, 50, 0.28f, 0.9f, 0.1f, 0.44f));				
			}
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(155), 49, 49))
			{	// duplicate button selected
				ChangeMessageArea(LanguageText.instance.Leditor_DuplicateBtn);
				sprites_effects.set(e_btnselected, 
						RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(206), 50, 50, 0.28f, 0.9f, 0.1f, 0.44f));
			}
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(207), 49, 49))
			{	// rotate button selected
				ChangeMessageArea(LanguageText.instance.Leditor_RotateBtn);
				sprites_effects.set(e_btnselected, 
						RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(258), 50, 50, 0.28f, 0.9f, 0.1f, 0.44f));
			}
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(259), 49, 49))
			{	// move button selected
				ChangeMessageArea(LanguageText.instance.Leditor_MoveBtn);
				sprites_effects.set(e_btnselected, 
						RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(310), 50, 50, 0.28f, 0.9f, 0.1f, 0.44f));
			}
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(311), 49, 49))
			{	// polygon button selected
				ChangeMessageArea(LanguageText.instance.Leditor_PolygonBtn);
				sprites_effects.set(e_btnselected, 
						RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(362), 50, 50, 0.28f, 0.9f, 0.1f, 0.44f));
				
				if(this.c_interface >= 700 && this.c_interface < 800)// if the interface state is related with the polygon button
				{
					if(this.c_interface==StandarValues.N_SELECTION)
						this.c_interface = StandarValues.Polygon_b_np;
					else if(this.c_interface==StandarValues.Polygon_b_mp)
					{
						// TODO: Add some processing to finish the polygon creation 
						this.c_interface = StandarValues.N_SELECTION;
					}		
				}
				else
					this.c_interface = SetCurrentInterfaceButton(X,Y);
				
			}
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(363), 49, 49))
			{	// complements button selected
				ChangeMessageArea(LanguageText.instance.Leditor_ComplementBtn);
				sprites_effects.set(e_btnselected, 
						RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(414), 50, 50, 0.28f, 0.9f, 0.1f, 0.44f));
			}
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(415), 49, 49))
			{	// powerups button selected
				ChangeMessageArea(LanguageText.instance.Leditor_PowerupBtn);
				sprites_effects.set(e_btnselected, 
						RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(466), 50, 50, 0.28f, 0.9f, 0.1f, 0.44f));
			}
			// Superior Panel
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(1), 49, 49))
			{	// file button selected
				ChangeMessageArea(LanguageText.instance.Leditor_FileBtn);
				sprites_effects.set(e_btnselected, 
						RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(51), 50, 50, 0.28f, 0.9f, 0.1f, 0.44f));
			}
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(60), Ut.TopLeftToMidCoordY(1), 99, 49))
			{	// back arrow text button selected
				if(this.msg_page>0)
					this.msg_page--;
			}
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(650), Ut.TopLeftToMidCoordY(1), 99, 49))
			{	// forward arrow text button selected
				if(this.msg_page<this.MessageArea.size-1)
					this.msg_page++;
			}
			else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(750), Ut.TopLeftToMidCoordY(1), 49, 49))
			{	// auxiliar button selected
				
				ChangeMessageArea(LanguageText.instance.Leditor_ChangeScreenBtn);
				
				sprites_effects.set(e_btnselected, 
						RectangleTouchEffect(Ut.TopLeftToMidCoordX(750), Ut.TopLeftToMidCoordY(1), 50, 50, 0.28f, 0.9f, 0.1f, 0.44f));
			}
			else
			{	// outside buttons
				//ChangeMessageArea(LanguageText.instance.Leditor_WelcomeText);
				sprites_effects.get(e_btnselected).setColor(1f,1f,1f,0f); // "hides this effect"
			}
		}
	}
	// ----
	
	// -- effect loaders
	/** When the screen is not touched, this methods hides some effects*/
	public void NotTouched()
	{
		// hides some effects
		sprites_effects.get(e_btnpressed).setColor(1f,1f,1f,0f);
		sprites_effects.get(e_hlpressed).setColor(1f,1f,1f,0f);
	}
	/** Show effects when an area inside a panel is being touched*/
	public void ButtonPressedEffect(int X, int Y)
	{
		if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(51), 49, 49))
		{	// selection button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(51+50), 50, 50);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(103), 49, 49))
		{	// delete button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(103+50), 50, 50);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(155), 49, 49))
		{	// duplicate button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(155+50), 50, 50);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(207), 49, 49))
		{	// rotate button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(207+50), 50, 50);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(259), 49, 49))
		{	// move button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(259+50), 50, 50);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(311), 49, 49))
		{	// polygon button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(311+50), 50, 50);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(363), 49, 49))
		{	// complements button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(363+50), 50, 50);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(415), 49, 49))
		{	// powerups button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(415+50), 50, 50);
		}
		// Superior Panel
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(1), 49, 49))
		{	// file button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(1+50), 50, 50);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(60), Ut.TopLeftToMidCoordY(1), 99, 49))
		{	// back arrow text button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(60), Ut.TopLeftToMidCoordY(1+50), 99, 49);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(650), Ut.TopLeftToMidCoordY(1), 99, 49))
		{	// forward arrow text button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(650), Ut.TopLeftToMidCoordY(1+50), 99, 49);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(750), Ut.TopLeftToMidCoordY(1), 49, 49))
		{	// auxiliar button pressed
			RectangleTouchEffect(Ut.TopLeftToMidCoordX(750), Ut.TopLeftToMidCoordY(1+50), 50, 50);
		}
		else
		{	// outside buttons

		}
	}
	/** Shows the effect of a preselected object (to help the player know what object is to be selected)
	 * <p> Note.- X, Y are not converted to other coordinate system*/
	public void EditionAreaPressedEffect(int X, int Y)
	{
		// TODO: Implement the effect "highlight pressed"
		// Example: when X,Y are over a polygon, its lines change to a certain color 
		//			and the	polygon's foreground take a translucent certain color
		// Example: If X,Y are over a powerup, a translucent rectangle marks it.
		
		// test effect
		sprites_effects.get(e_hlpressed).getTexture().dispose();	// sprites_effects(1) is reserved for this effect
		sprites_effects.set(e_hlpressed, CircleTouchEffect(X, Y, 8, 0.75f, 0.62f, 0.54f, 0.66f));
		//sprites_effects.set(e_hlpressed, RectangleTouchEffect(X, Y,10, 10, 0.75f,0.62f,0.54f,0.56f));
	}
	/** Loads the rectangle touch effect, an orange like color is loaded <p>
	 * Note.- The rectangle is drawn from left bottom corner*/
	public void RectangleTouchEffect(int x, int y, int width, int height)
	{	// the effect 0 is the rectangle touch effect
		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(0.72f, 0.50f, 0.1f, 0.67f );
		pixmap.fill();
		Texture texture = new Texture(pixmap);
		sprites_effects.set(e_btnpressed, new Sprite(texture));
		sprites_effects.get(e_btnpressed).setPosition(x, y);
		sprites_effects.get(e_btnpressed).setSize(width, height);
		pixmap.dispose();
	}
	/** returns a sprite containing the rectangle touch effect, a given color is loaded <p>
	 * Note.- The rectangle is drawn from left bottom corner*/
	public Sprite RectangleTouchEffect(int x, int y, int width, int height, float r, float g, float b, float a)
	{	// the effect 0 is the rectangle touch effect
		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(r, g, b, a);
		pixmap.fill();
		Texture texture = new Texture(pixmap);
		Sprite spr = new Sprite(texture);
		spr.setPosition(x, y);
		spr.setSize(width, height);
		pixmap.dispose();
		return spr;
	}
	/** returns a sprite containing the circle touch effect, a give color is loaded */
	public Sprite CircleTouchEffect(int x, int y, int radius, float r, float g, float b, float a)
	{
		Pixmap pixmap = new Pixmap(16, 16, Format.RGBA8888);
		pixmap.setColor(r, g, b, a);
		pixmap.fillCircle((int)(pixmap.getWidth()/2), (int)(pixmap.getHeight()/2), radius-1);
		pixmap.fillCircle((int)(pixmap.getWidth()/2), (int)(pixmap.getHeight()/2), radius-1);
		Texture texture = new Texture(pixmap);
		Sprite spr = new Sprite(texture);
		spr.setPosition(x-radius, y-radius);
		spr.setSize(radius+radius, radius+radius);
		pixmap.dispose();
		return spr;
	}
	// ----
	
	// -- Methods to process the selected option --
	/** It returns the interface state to the corresponding button*/
	public int SetCurrentInterfaceButton(int X, int Y)
	{
		if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(51), 49, 49))
		{	// selection button
			return(StandarValues.Selection_b_ns);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(103), 49, 49))
		{	// delete button 
			return(StandarValues.Delete_b_ns);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(155), 49, 49))
		{	// duplicate button 
			return(StandarValues.Duplicate_b_ns);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(207), 49, 49))
		{	// rotate button 
			return(StandarValues.Rotate_b_ns);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(259), 49, 49))
		{	// move button 
			return(StandarValues.Move_b_ns);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(311), 49, 49))
		{	// polygon button 
			return(StandarValues.Polygon_b_np);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(363), 49, 49))
		{	// complements button 
			return(StandarValues.Complements_b_w1);
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(415), 49, 49))
		{	// powerups button 
			return(StandarValues.Powerups_b_w1);
		}
		// Superior Panel
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(1), Ut.TopLeftToMidCoordY(1), 49, 49))
		{	// file button 
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(60), Ut.TopLeftToMidCoordY(1), 99, 49))
		{	// back arrow text button
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(650), Ut.TopLeftToMidCoordY(1), 99, 49))
		{	// forward arrow text button 
		}
		else if(Ut.InsideRectangle(X, Y, Ut.TopLeftToMidCoordX(750), Ut.TopLeftToMidCoordY(1), 49, 49))
		{	// auxiliar button
		}
		return -1;
	}

	/** Takes action when the polygon button is pressed*/

	// ----
	// -- Utils --
	private void ChangeMessageArea(String txt)
	{
		this.MessageArea.clear();
		this.MessageArea = Ut.SplitText(txt, this.fnt, StandarValues.LE_MSG_WIDTH);
		this.msg_page = 0; // first page of message area
		// hides the arrows icons
		this.sprites_nm.get(b_arrow_index).setColor(1f,1f,1f,0f);
		this.sprites_nm.get(f_arrow_index).setColor(1f,1f,1f,0f);
	}
	// ---	
	// -- Rendering methods --
	/** Render the panels and buttons */
	public void RenderWidgets(SpriteBatch batch)
	{
		for(Sprite sprite: sprites_nm)
		{
			if(sprite.getColor().toIntBits() <= 0xFFFFFF) // Do not draw totally translucent sprites
				sprite.draw(batch);
		}
	}
	/** Render the special effects */
	public void RenderEffects(SpriteBatch batch)
	{
		for(Sprite sprite: sprites_effects)
		{
			if(sprite.getColor().toIntBits() <= 0xFFFFFF) // Do not draw totally translucent sprites
				sprite.draw(batch);
		}
	}
	/** Render the text of the interface */
	public void RenderText(SpriteBatch batch)
	{
		BitmapFont text = Assets.instance.fonts.defaultNormal;
		text.setColor(0.9f,0.90f,0.97f,1f);
		text.setScale(1.62f, 1.5f);	// the text is a little wider to improve reading on small screens
		
		// -- rendering arrows of the text message area --
		int pages = this.MessageArea.size;
		if(pages>1) // pages selector
		{
			this.arrow_e_offset++;
			this.arrow_e_offset %= 100;
			//this.arrow_e_heigth %= 200;
			if(this.msg_page == 0) 
			{	// first message area page
				this.sprites_nm.get(f_arrow_index).setColor(1f,1f,1f, 1f);
				this.sprites_nm.get(f_arrow_index).setPosition(Ut.TopLeftToMidCoordX(705+(float)this.arrow_e_offset/5), 
						Ut.TopLeftToMidCoordY(30f));
				this.sprites_nm.get(b_arrow_index).setColor(1f,1f,1f,0f);
			}
			else if(this.msg_page == pages-1)
			{	// last message area page
				this.sprites_nm.get(b_arrow_index).setColor(1f,1f,1f, 1f);
				this.sprites_nm.get(b_arrow_index).setPosition(Ut.TopLeftToMidCoordX(75 - (float)this.arrow_e_offset/5), 
						Ut.TopLeftToMidCoordY(30f));
				this.sprites_nm.get(f_arrow_index).setColor(1f,1f,1f,0f);
			}
			else
			{	// can switch to both sides
				this.sprites_nm.get(b_arrow_index).setColor(1f,1f,1f, 1f);
				this.sprites_nm.get(f_arrow_index).setPosition(Ut.TopLeftToMidCoordX(705+(float)this.arrow_e_offset/5), 
						Ut.TopLeftToMidCoordY(30f));
				this.sprites_nm.get(f_arrow_index).setColor(1f,1f,1f, 1f);
				this.sprites_nm.get(b_arrow_index).setPosition(Ut.TopLeftToMidCoordX(75 - (float)this.arrow_e_offset/5), 
						Ut.TopLeftToMidCoordY(30f));
			}
		}
		// ----
	
		text.draw(batch, this.MessageArea.get(this.msg_page), Ut.TopLeftToMidCoordX(53f), Ut.TopLeftToMidCoordY(15f));

		//text.drawWrapped(batch, this.MessageArea, Ut.TopLeftToMidCoordX(65f), Ut.TopLeftToMidCoordY(15f), 672);
	}
	// ----
}
