/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tuxfamily.chwnku.io.Levels;

import com.badlogic.gdx.utils.Array;

/** The level object, contains at least one level screen.<p> 
 * All meta data about the level is placed here.*/
public class Level {
	protected Array<LevelScreen> screens;
	protected String Autor;
	protected String Description;
	protected String[] SoundtrackList;
	protected boolean OrderType;
	protected String filename;
	protected int CurrentScreen; 
	
	
	/** */
	public Level() 
	{
		SoundtrackList[12] = new String();
		NewLevel();
	}
	
	public void NewLevel() // A new level that has nothing but an empty screen
	{
		screens.clear();
		screens.add(new LevelScreen()); // adds an empty level screen
		Autor = "";
		Description = "";
		SoundtrackList[0 >>> 11] = ""; // what does >>> do?
		OrderType = false; // random track to be played
		filename = "";
		CurrentScreen = 0;
	}
}
