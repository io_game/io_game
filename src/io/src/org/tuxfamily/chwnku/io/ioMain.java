/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.tuxfamily.chwnku.io;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;

import com.badlogic.gdx.ApplicationListener;
import org.tuxfamily.chwnku.io.game.WorldController;
import org.tuxfamily.chwnku.io.game.WorldRenderer;

import com.badlogic.gdx.assets.AssetManager;
import org.tuxfamily.chwnku.io.game.Assets;;

public class ioMain implements ApplicationListener {

	public static final String TAG = ioMain.class.getName();

	private WorldController worldController;
	private WorldRenderer worldRenderer;

	private boolean paused;

	@Override
	public void create() 
	{
		// Set Libgdx log level to DEBUG
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		// Load assets
		Assets.instance.init(new AssetManager());
		// Initialize controller and renderer
		worldController = new WorldController();
		worldRenderer = new WorldRenderer(worldController);
		// Game world is acive on start
		paused = false;
	}

	@Override
	public void resize(int width, int height) 
	{
		worldRenderer.resize(width, height);
	}

	@Override
	public void render() 
	{
		if (!paused)
		{
			// Update game world by the time that has passed
			// since last rendered frame.
			worldController.update(Gdx.graphics.getDeltaTime());
			Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		}
		Gdx.gl.glClearColor(0x74/255f, 0x7A/255f, 0xCF/255f, 0xFF/255f);
		/*
		// Sets the clear screen color to: Cornflower Blue
		Gdx.gl.glClearColor(0x64/255.0f, 0x95/255.0f, 0xed/255.0f, 0xff/255.0f);
		// Clears the screen
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		// Render game world to screen
		 */
		worldRenderer.render();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		paused = true;
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		Assets.instance.init(new AssetManager()); //to reload the assets in memory
		paused = false;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		worldRenderer.dispose();
		Assets.instance.dispose(); //clean memory used by the assets

	}

}
