/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.tuxfamily.chwnku.io.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;

import org.tuxfamily.chwnku.io.utilities.StandarValues;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/* 
 * This class is used to manage assets such as images in a way that they
 * are automatically disposed when not in use and reloaded when needed and
 * to asynchronously load new assets (loading assents in the background) 
 * 
 */
public class Assets implements Disposable, AssetErrorListener{

	public static final String TAG = Assets.class.getName();
	public static final Assets instance = new Assets();
	private AssetManager assetManager;

	public AssetMenuScreenBg menu_screen_bg;
	public AssetMenuScreenLogo menu_screen_logo;
	public AssetMenuBrids menu_screen_birds;
	public AssetFonts fonts;
	public AssetBee menu_screen_bee;
	
	public AssetsPanels leditor_panels;
	public AssetsIcons leditor_icons;

	// singleton: prevent instantiation from other classes
	private Assets () {}

	public void init (AssetManager assetManager) 
	{
		this.assetManager = assetManager;
		// set asset manager error handler
		assetManager.setErrorListener(this);
		// load texture atlas
		assetManager.load(StandarValues.TEXTURE_ATLAS_MENU_SCREEN,	TextureAtlas.class);
		assetManager.load(StandarValues.TEXTURE_ATLAS_ANIMATE,  TextureAtlas.class);
		// start loading assets and wait until finished
		assetManager.finishLoading();
		Gdx.app.debug(TAG, "# of assets loaded: " + assetManager.getAssetNames().size);
		for (String a : assetManager.getAssetNames()) Gdx.app.debug(TAG, "asset: " + a);

		TextureAtlas atlas_MainMenuScreen = assetManager.get(StandarValues.TEXTURE_ATLAS_MENU_SCREEN);
		TextureAtlas atlas_animate = assetManager.get (StandarValues.TEXTURE_ATLAS_ANIMATE);

		// enable texture filtering for pixel smoothing
		for (Texture t : atlas_MainMenuScreen.getTextures())
			t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		for (Texture t : atlas_animate.getTextures())
			t.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		// create game resource objects
		menu_screen_bg = new AssetMenuScreenBg(atlas_MainMenuScreen);
		menu_screen_logo = new AssetMenuScreenLogo(atlas_MainMenuScreen);
		menu_screen_birds = new AssetMenuBrids(atlas_animate);
		menu_screen_bee = new AssetBee(atlas_animate);
		leditor_icons =  new AssetsIcons(atlas_MainMenuScreen);
		leditor_panels =  new AssetsPanels(atlas_MainMenuScreen);
		fonts = new AssetFonts();
	}

	@Override
	public void error(String fileName, Class type, Throwable throwable) 
	{
		// here code to avoid the app crash

		Gdx.app.error(TAG, "Couldn't load asset '" + fileName + "'", (Exception)throwable);

	}
	@Override
	public void dispose()
	{
		assetManager.dispose();
		fonts.defaultNormal.dispose();
	}

	// -- assets for menu screen --
	public class AssetMenuScreenBg{
		public final AtlasRegion background;
		public final AtlasRegion cactus;
		public AssetMenuScreenBg(TextureAtlas atlas)
		{
			this.background = atlas.findRegion("images/backgrounds/bg_io_menu");
			this.cactus = atlas.findRegion("images/msc/io_cactus");
		}
	}
	public class AssetMenuScreenLogo{
		public final AtlasRegion logo;
		public AssetMenuScreenLogo(TextureAtlas atlas)
		{
			this.logo = atlas.findRegion("images/msc/logo");
		}
	}

	public class AssetMenuBrids{
		public final AtlasRegion bird1_1;
		public final AtlasRegion bird1_2;
		public final AtlasRegion bird1_3;
		public final AtlasRegion bird2_1;
		public final AtlasRegion bird2_2;
		public final AtlasRegion bird2_3;
		public AssetMenuBrids(TextureAtlas atlas)
		{
			this.bird1_1 = atlas.findRegion("bird1-1");
			this.bird1_2 = atlas.findRegion("bird1-2");
			this.bird1_3 = atlas.findRegion("bird1-3");
			this.bird2_1 = atlas.findRegion("bird2-1");
			this.bird2_2 = atlas.findRegion("bird2-2");
			this.bird2_3 = atlas.findRegion("bird2-3");
		}
	}

	public class AssetBee{
		public final AtlasRegion bee1_1;

		public AssetBee(TextureAtlas atlas)
		{
			this.bee1_1 = atlas.findRegion("bee1-1");
		}

	}

	public class AssetFonts{
		public BitmapFont defaultNormal;
		public AssetFonts ()
		{
			// create two fonts using IO's 14px bitmap font
			// *) it looks by default inside the android's directory folder "assets"
			Gdx.app.log(TAG, "Loading file: fonts/io_fnt-14.fnt");
			defaultNormal = new BitmapFont(Gdx.files.internal("fonts/io_fnt-14.fnt"), false);
			// set font sizes
			defaultNormal.setScale(1.6f);
			// enable linear texture filtering for smooth fonts
			defaultNormal.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.MipMap);
		}
	}
	// ----
	// -- Assets for Level Editor --
	// panels
	public class AssetsPanels{
		public final AtlasRegion left_panel;
		public final AtlasRegion superior_panel;
		public AssetsPanels(TextureAtlas atlas)
		{
			this.left_panel = atlas.findRegion("images/msc/left_panel");
			this.superior_panel = atlas.findRegion("images/msc/superior_panel");
		}
	}
	// icons
	public class AssetsIcons{
		public final AtlasRegion selection;
		public final AtlasRegion delete;
		public final AtlasRegion duplicate;
		public final AtlasRegion rotate;
		public final AtlasRegion move;
		public final AtlasRegion polygon;
		public final AtlasRegion complements;
		public final AtlasRegion powerups;
		public final AtlasRegion file;
		public final AtlasRegion change_screen;
		public final AtlasRegion properties;
		public final AtlasRegion b_arrow_t;
		public final AtlasRegion f_arrow_t;
		public AssetsIcons(TextureAtlas atlas)
		{
			this.selection = atlas.findRegion("images/icons/select");
			this.delete = atlas.findRegion("images/icons/delete");
			this.duplicate = atlas.findRegion("images/icons/duplicate");
			this.rotate = atlas.findRegion("images/icons/rotate");
			this.move = atlas.findRegion("images/icons/move");
			this.polygon = atlas.findRegion("images/icons/poligon");
			this.complements = atlas.findRegion("images/icons/ornaments");
			this.powerups = atlas.findRegion("images/icons/powerups");
			this.file = atlas.findRegion("images/icons/file");
			this.change_screen = atlas.findRegion("images/icons/change_screen");
			this.properties = atlas.findRegion("images/icons/properties");
			this.b_arrow_t = atlas.findRegion("images/icons/b_arrow_t");
			this.f_arrow_t = atlas.findRegion("images/icons/f_arrow_t");
		}
	}
	// 
	// ----
}
