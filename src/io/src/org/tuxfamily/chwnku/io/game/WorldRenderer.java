/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.tuxfamily.chwnku.io.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

import org.tuxfamily.chwnku.io.screens.ScreensManager;
import org.tuxfamily.chwnku.io.utilities.StandarValues;

public class WorldRenderer implements Disposable {
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private WorldController worldController;
	
	public WorldRenderer(WorldController worldController)
	{
		this.worldController = worldController;
		init();
	}
	
	public void init()
	{
		batch = new SpriteBatch();
		// defining the world size see StandarValues
		camera = new OrthographicCamera(StandarValues.VIEWPORT_WIDTH,StandarValues.VIEWPORT_HEIGHT);
		camera.position.set(0, 0, 0);
		camera.update();
		Gdx.app.log("Dimensions :", Gdx.graphics.getWidth() + " x " + Gdx.graphics.getHeight() );
	}
	
	public void render()
	{
		// render the game screens
		renderScreens();
	}
	private void renderScreens() 
	{
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		// Screens drawing
		ScreensManager.instance.RenderCurrent(batch);
		
		// ends the sprite batch drawing
		batch.end();
	}
	public void resize(int width, int height)
	{
		camera.viewportHeight = StandarValues.SCREEN_WIDTH;
		camera.viewportHeight = StandarValues.SCREEN_HEIGHT;
		//setViewport(StandarValues.SCREEN_WIDTH, StandarValues.VIEWPORT_HEIGHT, false);
		//camera.viewportWidth = (StandarValues.VIEWPORT_HEIGHT / height) * width;
		camera.update();
	}
	
	@Override
	public void dispose()
	{
		batch.dispose();	
	}
	
	/**  code-snipet from Stage class of com.badlogic.gdx.scenes.scens2d :) */
	public void setViewport (float width, float height, boolean keepAspectRatio) {
		float W;
		float H;
		float gutterWidth;
		float gutterHeight;
		if (keepAspectRatio) {
			float screenWidth = Gdx.graphics.getWidth();
			float screenHeight = Gdx.graphics.getHeight();
			if (screenHeight / screenWidth < height / width) {
				float toScreenSpace = screenHeight / height;
				float toViewportSpace = height / screenHeight;
				float deviceWidth = width * toScreenSpace;
				float lengthen = (screenWidth - deviceWidth) * toViewportSpace;
				W = width + lengthen;
				H = height;
				gutterWidth = lengthen / 2;
				gutterHeight = 0;
			} else {
				float toScreenSpace = screenWidth / width;
				float toViewportSpace = width / screenWidth;
				float deviceHeight = height * toScreenSpace;
				float lengthen = (screenHeight - deviceHeight) * toViewportSpace;
				H = height + lengthen;
				W = width;
				gutterWidth = 0;
				gutterHeight = lengthen / 2;
			}
		} else {
			W = width;
			H = height;
			gutterWidth = 0;
			gutterHeight = 0;
		}

		//float centerX = W / 2;
		//float centerY = H / 2;

		//camera.position.set(centerX, centerY, 0);
		camera.viewportWidth = W;
		camera.viewportHeight = H;
	}
	
}
