/*
 * LICENCE
io_game 
Copyright (C) 2015 Rodrigo Garcia 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.tuxfamily.chwnku.io.game;

import org.tuxfamily.chwnku.io.screens.ScreensManager;

public class WorldController {

	private static final String TAG =  WorldController.class.getName();


	public WorldController()
	{
		init();
	}

	public void init()
	{
		ScreensManager.instance.init();
	}

	public void update (float deltaTime)
	{
		ScreensManager.instance.UpdateCurrent(deltaTime);
	}


}
 